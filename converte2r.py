names = ['mary','joe','john','gary','rolf','chris','melissa','rachel','sally','sue']
number = 1
global example



with open('data2.txt') as f:
    lines = f.readlines()
    
def populate():
    location = ""
    person = ""
    fluent = ""
    event = ""
    count = 0
    tp = 0
    example = "\nbegin(model(f"+str(number)+")).\n"
    for line in lines:
        if count == 3:
            example +=  "end(model(f"+str(number)+")).\n"
            break
        if "NNP " in line:
            parts = line.split()
            test = ''.join(parts[-1:])
            test = test.replace(')', " ")
            person += test.strip().lower()
           # test = parts[2][:-2]
           #test = test.lower()
           # if test in names:
           #     person = test
           # print(person)
        if "NN " in line:
            parts = line.split()
            test = ''.join(parts[-1:])
            test = test.replace(')', " ")
            location += test.strip().lower()
        if "VBN " in line:
            parts = line.split()
            test = ''.join(parts[-1:])
            test = test.replace(')', " ")
            fluent += test.strip().lower()
        if "VBZ " in line:
            parts = line.split()
            test = ''.join(parts[-1:])
            test = test.replace(')', " ")
            if len(test) > 3:
                event += test.strip().lower()
            else:
                event = ""
        if line == "\n":
            tp += 1
            if fluent == "" and event == "":
                fluent += "located"
                count += 1
                example += "holds_at([fluent:"+fluent+", pers:mary, loc:kitchen], tp:"+str(tp)+").\n"
                del location
                del person
                del fluent
                del event
                location = ""
                person = ""
                fluent = ""
                event = ""
                continue;
            if event == "":
                count += 1
                example += "holds_at([fluent:"+fluent+", pers:"+person+", loc:"+location+"], tp:"+str(tp)+").\n"
                del location
                del person
                del fluent
                del event
                location = ""
                person = ""
                fluent = ""
                event = ""
                continue;
            if fluent == "":
                count += 1
                example += "happens_at([event:"+event[:-1]+", pers:"+person+", loc:"+location+"], tp:"+str(tp)+").\n"
                del location
                del person
                del fluent
                del event
                location = ""
                person = ""
                fluent = ""
                event = ""
                continue;
    return example


with open('learn_effect_axioms.pl', 'a') as myfile:
    myfile.write(populate())
        
        
