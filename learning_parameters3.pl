% ======================================================================
% Learning Probabilistic Effect Axioms
%
% Authors:
%   Christopher Chan (Macquarie University, Sydney)
%   Rolf Schwitter   (Macquarie University, Sydney)
%
% Date:
%   20th October 2017
% ======================================================================


/** <examples>

?- learn_parameters(C).

*/
:- use_module(library(slipcover)).

:- if(current_predicate(use_rendering/1)).
:- use_rendering(c3).
:- use_rendering(lpad).
:- endif.


:- sc.

:- set_sc(max_var, 4).
:- set_sc(verbosity, 3).
:- set_sc(megaex_bottom, 37).
:- set_sc(depth_bound, false).
:- set_sc(neg_ex, given).


% ----------------------------------------------------------------------
% Language bias: initiated_at/3
% ----------------------------------------------------------------------

output(initiated_at/3).
input(happens_at/2).
input(holds_at/2).
% input('<'/2).

determination(initiated_at/3, happens_at/2).
determination(initiated_at/3, holds_at/2).
% determination(initiated_at/3, '<'/2).

% ----------------------------------------------------------------------
% Language bias: terminated_at/2
% ----------------------------------------------------------------------

output(terminated_at/3).
input(happens_at/2).

determination(terminated_at/3, happens_at/2).

% ----------------------------------------------------------------------
% Background information
% ----------------------------------------------------------------------
:- begin_in.

initiated_at([fluent:located, pers:P, loc:L2], tp:T1, tp:T2):0.5 :-
   happens_at([event:walk, pers:P, loc:L1, loc:L2], tp:T1),
   T1 < T2,  \+ L1 = L2.

initiated_at([fluent:located, pers:P, loc:L2], tp:T1, tp:T2):0.5 :-
  happens_at([event:drive, pers:P, loc:L1, loc:L2], tp:T1),
  T1 < T2, \+ L1 = L2,
 \+ holds_at([fluent:broken, obj:B], tp:T1),
  holds_at([fluent:located, pers:P, loc:L1], tp:T1),
 holds_at([fluent:located, obj:B, loc:L1], tp:T1).

initiated_at([fluent:located, obj:B, loc:L2], tp:T1, tp:T2):0.5 :-
  happens_at([event:drive, pers:P, loc:L1, loc:L2], tp:T1),
  T1 < T2, \+ L1 = L2,
 \+ holds_at([fluent:broken, obj:B], tp:T1),
  holds_at([fluent:located, pers:P, loc:L1], tp:T1),
 holds_at([fluent:located, obj:B, loc:L1], tp:T1).

initiated_at([fluent:broken, obj:B], tp:T1, tp:T2):0.5 :-
    happens_at([event:break, obj:B], tp:T1),
    T1 < T2,
    holds_at([fluent:damaged, obj:B], tp:T1).

initiated_at([fluent:carrying, pers:P, obj:O], tp:T1, tp:T2):0.5 :-
  happens_at([event:pick_up, pers:P, obj:O], tp:T1),
  T1 < T2,
  holds_at([fluent:located, pers:P, loc:L], tp:T1),
  holds_at([fluent:located, obj:O, loc:L], tp:T1).

initiated_at([fluent:located, obj:O, loc:L2], tp:T1, tp:T2):0.5 :-
  happens_at([event:walk, pers:P, loc:L1, loc:L2], tp:T1),
  T1 < T2, \+ L1 = L2,
  holds_at([fluent:carrying, pers:P, obj:O], tp:T1).

initiated_at([fluent:located, obj:O, loc:L2], tp:T1, tp:T2):0.5 :-
  happens_at([event:drive, pers:P, loc:L1, loc:L2], tp:T1),
  T1 < T2, \+ L1 = L2,
  holds_at([fluent:carrying, pers:P, obj:O], tp:T1).

terminated_at([fluent:located,pers:P,loc:L1],tp:T1, tp:T2):0.5 :-
   happens_at([event:walk,pers:P,loc:L1,loc:L2],tp:T1),
   T1 < T2,  \+ L1 = L2.

terminated_at([fluent:located, pers:P, loc:L1], tp:T1, tp:T2):0.5 :-
  happens_at([event:drive, pers:P, loc:L1, loc:L2], tp:T1),
  T1 < T2, \+ L1 = L2,
 \+ holds_at([fluent:broken, obj:B], tp:T1),
  holds_at([fluent:located, pers:P, loc:L1], tp:T1),
 holds_at([fluent:located, obj:B, loc:L1], tp:T1).

terminated_at([fluent:located, obj:B, loc:L1], tp:T1, tp:T2):0.5 :-
  happens_at([event:drive, pers:P, loc:L1, loc:L2], tp:T1),
  T1 < T2, \+ L1 = L2,
 \+ holds_at([fluent:broken, obj:B], tp:T1),
  holds_at([fluent:located, pers:P, loc:L1], tp:T1),
 holds_at([fluent:located, obj:B, loc:L1], tp:T1).

terminated_at([fluent:carrying, pers:P, obj:O], tp:T1, tp:T2):0.5 :-
  happens_at([event:put_down, pers:P, obj:O], tp:T1),
  holds_at([fluent:carrying, pers:P, obj:O], tp:T1),
  T1 < T2.

terminated_at([fluent:damaged, obj:B], tp:T1, tp:T2):0.5 :-
    holds_at([fluent:damaged, obj:B], tp:T1),
    happens_at([event:fix, pers:_P, obj:B], tp:T1),
    T1 < T2,
    \+ holds_at([fluent:damaged, obj:B], tp:T2).

terminated_at([fluent:located, obj:O, loc:L1], tp:T1, tp:T2):0.5 :-
  happens_at([event:walk, pers:P, loc:L1, loc:L2], tp:T1),
  T1 < T2, \+ L1 = L2,
  holds_at([fluent:carrying, pers:P, obj:O], tp:T1).


terminated_at([fluent:located, obj:O, loc:L1], tp:T1, tp:T2):0.5 :-
  happens_at([event:drive, pers:P, loc:L1, loc:L2], tp:T1),
  T1 < T2, \+ L1 = L2,
  holds_at([fluent:carrying, pers:P, obj:O], tp:T1).


:- end_in.

:- begin_bg.

% location([loc: bathroom]).
% location([loc: bedroom]).
% location([loc: garage]).
% location([loc: hallway]).
% location([loc: kitchen]).
% location([loc: office]).
% location([loc: yard]).
% location([loc: store]).

:- end_bg.


% Example generation:
% ----------------------------------------------------------------------
% Positive effect axiom
% ----------------------------------------------------------------------
%

% walks + located at
initiated_at(ID, [fluent:F2, pers:P, loc:L2], tp:T2, tp:T3) :-
  holds_at(ID, [fluent:_F1, pers:P, loc:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
  T1 < T2,
  holds_at(ID, [fluent:F2, pers:P, loc:L2], tp:T3),
  T2 < T3.
 
 neg(initiated_at(ID, [fluent:F2, pers:P, loc:L2], tp:T2, tp:T3)) :-
  holds_at(ID, [fluent:_F1, pers:P, loc:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
  T1 < T2,
  neg(holds_at(ID, [fluent:F2, pers:P, loc:L2], tp:T3)),
  T2 < T3.

% walks + located at
initiated_at(ID, [fluent:F2, obj:O, loc:L2], tp:T2, tp:T3) :-
  holds_at(ID, [fluent:_F1, pers:P, obj:O], tp:T2),
  happens_at(ID, [event:_E, pers:P, loc:_L1, loc:L2], tp:T2),
  holds_at(ID, [fluent:F2, obj:O, loc:L2], tp:T3),
  T2 < T3.
 
 neg(initiated_at(ID, [fluent:F2, obj:O, loc:L2], tp:T2, tp:T3)) :-
  holds_at(ID, [fluent:_F1, pers:P, obj:O], tp:T2),
  happens_at(ID, [event:_E, pers:P, loc:_L1, loc:L2], tp:T2),
  neg(holds_at(ID, [fluent:F2, obj:O, loc:L2], tp:T3)),
  T2 < T3.

% break > broken
initiated_at(ID, [fluent:F1, obj:O], tp:T2, tp:T3) :-
    %damaged
  holds_at(ID, [fluent:_F2, obj:O], tp:T2),
  %T1< T2,
  happens_at(ID, [event:_E, obj:O], tp:T2),
    %broken
  holds_at(ID, [fluent:F1, obj:O], tp:T3),
  T2 < T3.

neg(initiated_at(ID, [fluent:F1, obj:O], tp:T2, tp:T3)):-
    %damaged
  holds_at(ID, [fluent:_F2, obj:O], tp:T2),
%  T1 < T2,
  happens_at(ID, [event:_E, obj:O], tp:T2),
    %broken
  neg(holds_at(ID, [fluent:F1, obj:O], tp:T3)),
  T2 < T3.

% drive
initiated_at(ID, [fluent:F2, pers:P, loc:L2], tp:T2, tp:T3) :-
   neg(holds_at(ID, [fluent:_F1, obj:O], tp:T2)),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
    holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2),
  holds_at(ID, [fluent:F2, pers:P, loc:L1], tp:T2),
 % T1 < T2,
  holds_at(ID, [fluent:F2, pers:P, loc:L2], tp:T3),
  T2 < T3.
 
 neg(initiated_at(ID, [fluent:F2, pers:P, loc:L2], tp:T2, tp:T3)) :-
   neg(holds_at(ID, [fluent:_F1, obj:O], tp:T2)),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
        holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2),
  holds_at(ID, [fluent:F2, pers:P, loc:L1], tp:T2),
%  T1 < T2,
  neg(holds_at(ID, [fluent:F2, pers:P, loc:L2], tp:T3)),
  T2 < T3.

% drive
initiated_at(ID, [fluent:F2, obj:O, loc:L2], tp:T2, tp:T3) :-
   neg(holds_at(ID, [fluent:_F1, obj:O], tp:T2)),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
    holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2),
  holds_at(ID, [fluent:F2, pers:P, loc:L1], tp:T2),
 % T1 < T2,
  holds_at(ID, [fluent:F2, obj:O, loc:L2], tp:T3),
  T2 < T3.
 
 neg(initiated_at(ID, [fluent:F2, obj:O, loc:L2], tp:T2, tp:T3)) :-
   neg(holds_at(ID, [fluent:_F1, obj:O], tp:T2)),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
        holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2),
  holds_at(ID, [fluent:F2, pers:P, loc:L1], tp:T2),
%  T1 < T2,
  neg(holds_at(ID, [fluent:F2, obj:O, loc:L2], tp:T3)),
  T2 < T3.

%pick up
initiated_at(ID, [fluent:F1, pers:P, obj:O], tp:T2, tp:T3) :-
  happens_at(ID, [event:_E, pers:P, obj:O], tp:T2),
  holds_at(ID, [fluent:F2, pers:P, loc:L1], tp:T2),
  holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2),
  holds_at(ID, [fluent:F1, pers:P, obj:O], tp:T3),
  T2 < T3.
 
 neg(initiated_at(ID, [fluent:F1, pers:P, obj:O], tp:T2, tp:T3)) :-
  happens_at(ID, [event:_E, pers:P, obj:O], tp:T2),
  holds_at(ID, [fluent:F2, pers:P, loc:L1], tp:T2),
  holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2),
  neg(holds_at(ID, [fluent:F1, pers:P, obj:O], tp:T3)),
  T2 < T3.

% Example generation:
% ----------------------------------------------------------------------
% Negative effect axiom
% ----------------------------------------------------------------------
% walk > located
terminated_at(ID, [fluent:F1, pers:P, loc:L1], tp:T2, tp:T3) :-
  holds_at(ID, [fluent:F1, pers:P, loc:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
  T1 < T2,
  holds_at(ID, [fluent:_F2, pers:P, loc:L2], tp:T3),
  T2 < T3.

neg(terminated_at(ID, [fluent:F1, pers:P, loc:L1], tp:T2, tp:T3)) :-
  holds_at(ID, [fluent:F1, pers:P, loc:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
  T1 < T2,
  neg(holds_at(ID, [fluent:_F2, pers:P, loc:L2], tp:T3)),
  T2 < T3.

% drive
terminated_at(ID, [fluent:F2, pers:P, loc:L1], tp:T2, tp:T3) :-
   neg(holds_at(ID, [fluent:_F1, obj:O], tp:T2)),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:_L2], tp:T2),
    holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2),
  holds_at(ID, [fluent:F2, pers:P, loc:L1], tp:T2),
 % T1 < T2,
  neg(holds_at(ID, [fluent:F2, pers:P, loc:L1], tp:T3)),
  T2 < T3.
 
 neg(terminated_at(ID, [fluent:F2, pers:P, loc:L1], tp:T2, tp:T3)) :-
   neg(holds_at(ID, [fluent:_F1, obj:O], tp:T2)),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:_L2], tp:T2),
        holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2),
  holds_at(ID, [fluent:F2, pers:P, loc:L1], tp:T2),
%  T1 < T2,
  holds_at(ID, [fluent:F2, pers:P, loc:L1], tp:T3),
  T2 < T3.

% drive - car
terminated_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2, tp:T3) :-
   neg(holds_at(ID, [fluent:_F1, obj:O], tp:T2)),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:_L2], tp:T2),
    holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2),
  holds_at(ID, [fluent:F2, pers:P, loc:L1], tp:T2),
 % T1 < T2,
  neg(holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T3)),
  T2 < T3.
 
 neg(terminated_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2, tp:T3)) :-
   neg(holds_at(ID, [fluent:_F1, obj:O], tp:T2)),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:_L2], tp:T2),
        holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2),
  holds_at(ID, [fluent:F2, pers:P, loc:L1], tp:T2),
%  T1 < T2,
  holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T3),
  T2 < T3.


% break > broken
terminated_at(ID, [fluent:F1, obj:O], tp:T2, tp:T3) :-
    %damaged
  holds_at(ID, [fluent:F1, obj:O], tp:T2),
  %T1< T2,
  happens_at(ID, [event:_E, pers:_P, obj:O], tp:T2),
    %broken
  neg(holds_at(ID, [fluent:F1, obj:O], tp:T3)),
  T2 < T3.

neg(terminated_at(ID, [fluent:F1, obj:O], tp:T2, tp:T3)):-
    %damaged
  holds_at(ID, [fluent:F1, obj:O], tp:T2),
%  T1 < T2,
  happens_at(ID, [event:_E, pers:_P, obj:O], tp:T2),
    %broken
  holds_at(ID, [fluent:F1, obj:O], tp:T3),
  T2 < T3.

%put_down
terminated_at(ID, [fluent:F1, pers:P, obj:O], tp:T2, tp:T3) :-
  holds_at(ID, [fluent:F1, pers:P, obj:O], tp:T2),
  happens_at(ID, [event:_E, pers:P, obj:O], tp:T2),
  neg(holds_at(ID, [fluent:F1, pers:P, obj:O], tp:T3)),
  T2 < T3.

neg(terminated_at(ID, [fluent:F1, pers:P, obj:O], tp:T2, tp:T3)) :-
  holds_at(ID, [fluent:F1, pers:P, obj:O], tp:T2),
  happens_at(ID, [event:_E, pers:P, obj:O], tp:T2),
  holds_at(ID, [fluent:F1, pers:P, obj:O], tp:T3),
  T2 < T3.

% walks + located at
terminated_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2, tp:T3) :-
  holds_at(ID, [fluent:_F1, pers:P, obj:O], tp:T2),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:_L2], tp:T2),
  neg(holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T3)),
  T2 < T3.
 
 neg(terminated_at(ID, [fluent:F2, obj:O, loc:L1], tp:T2, tp:T3)) :-
  holds_at(ID, [fluent:_F1, pers:P, obj:O], tp:T2),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:_L2], tp:T2),
  holds_at(ID, [fluent:F2, obj:O, loc:L1], tp:T3),
  T2 < T3.

% ----------------------------------------------------------------------
% Example interpretations
% ----------------------------------------------------------------------
begin(model(f1)).
holds_at([fluent:located, obj:car, loc:yard], tp:0).
holds_at([fluent:damaged, obj:car], tp:1).
happens_at([event:break, obj:car], tp:1).
neg(holds_at([fluent:broken, obj:car], tp:2)).
end(model(f1)).

begin(model(f2)).
holds_at([fluent:located, obj:car, loc:yard], tp:0).
holds_at([fluent:damaged, obj:car], tp:1).
happens_at([event:break, obj:car], tp:1).
holds_at([fluent:broken, obj:car], tp:3).
end(model(f2)).

begin(model(f3)).
holds_at([fluent:located, obj:car, loc:yard], tp:0).
holds_at([fluent:damaged, obj:car], tp:1).
happens_at([event:break, obj:car], tp:1).
holds_at([fluent:broken, obj:car], tp:5).
end(model(f3)).

begin(model(f4)).
holds_at([fluent:located, obj:car, loc:yard], tp:0).
holds_at([fluent:damaged, obj:car], tp:1).
happens_at([event:break, obj:car], tp:1).
holds_at([fluent:broken, obj:car], tp:4).
end(model(f4)).

begin(model(f5)).
holds_at([fluent:located, pers:mary, loc:yard], tp:0).
happens_at([event:walk, pers:mary, loc:yard, loc:kitchen], tp:3).
neg(holds_at([fluent:located, pers:mary, loc:yard], tp:4)).
holds_at([fluent:located, pers:mary, loc:kitchen], tp:5).
end(model(f5)).

begin(model(f6)).
holds_at([fluent:located, pers:mary, loc:yard], tp:0).
happens_at([event:walk, pers:mary, loc:yard, loc:garage], tp:4).
neg(holds_at([fluent:located, pers:mary, loc:garage], tp:5)).
holds_at([fluent:located, pers:mary, loc:yard], tp:6).
end(model(f6)).

begin(model(f7)).
holds_at([fluent:located, pers:mary, loc:yard], tp:1).
happens_at([event:walk, pers:mary, loc:yard, loc:kitchen], tp:2).
holds_at([fluent:located, pers:mary, loc:kitchen], tp:3).
neg(holds_at([fluent:located, pers:mary, loc:yard], tp:4)).
end(model(f7)).

begin(model(f8)).
holds_at([fluent:located, pers:sue, loc:hallway], tp:1).
happens_at([event:walk, pers:sue, loc:hallway, loc:office], tp:3).
neg(holds_at([fluent:located, pers:sue, loc:hallway], tp:4)).
holds_at([fluent:located, pers:sue, loc:office], tp:5).
end(model(f8)).

begin(model(f9)).
holds_at([fluent:located, pers:sue, loc:bedroom], tp:1).
happens_at([event:walk, pers:sue, loc:bedroom,  loc:bathroom], tp:2).
neg(holds_at([fluent:located, pers:sue, loc:bedroom], tp:3)).
holds_at([fluent:located, pers:sue, loc:bathroom], tp:4).
end(model(f9)).

begin(model(f10)).
holds_at([fluent:located, pers:mary, loc:bedroom], tp:1).
holds_at([fluent:located, obj:book, loc:bedroom], tp:1).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
holds_at([fluent:carrying, pers:mary, obj:book], tp:5).
end(model(f10)).

begin(model(f11)).
holds_at([fluent:located, pers:mary, loc:bedroom], tp:0).
holds_at([fluent:located, obj:book, loc:bedroom], tp:0).
happens_at([event:pick_up, pers:mary, obj:book], tp:0).
holds_at([fluent:carrying, pers:mary, obj:book], tp:2).
end(model(f11)).

begin(model(f12)).
holds_at([fluent:located, pers:mary, loc:office], tp:3).
holds_at([fluent:located, obj:book, loc:office], tp:3).
happens_at([event:pick_up, pers:mary, obj:book], tp:3).
holds_at([fluent:carrying, pers:mary, obj:book], tp:5).
end(model(f12)).

begin(model(f13)).
holds_at([fluent:located, pers:mary, loc:bathroom], tp:1).
holds_at([fluent:located, obj:book, loc:bathroom], tp:1).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
holds_at([fluent:carrying, pers:mary, obj:book], tp:2).
end(model(f13)).

begin(model(f14)).
holds_at([fluent:located, pers:mary, loc:garage], tp:1).
holds_at([fluent:located, obj:book, loc:garage], tp:1).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
neg(holds_at([fluent:carrying, pers:mary, obj:book], tp:4)).
end(model(f14)).

begin(model(f15)).
holds_at([fluent:damaged, obj:car], tp:1).
happens_at([event:fix, pers:bob, obj:car], tp:1).
holds_at([fluent:damaged, obj:car], tp:3).
end(model(f15)).

begin(model(f16)).
holds_at([fluent:damaged, obj:car], tp:1).
happens_at([event:fix, pers:eve, obj:car], tp:1).
neg(holds_at([fluent:damaged, obj:car], tp:4)).
end(model(f16)).

begin(model(f17)).
holds_at([fluent:damaged, obj:car], tp:1).
happens_at([event:fix, pers:alice, obj:car], tp:1).
neg(holds_at([fluent:damaged, obj:car], tp:5)).
end(model(f17)).

begin(model(f18)).
holds_at([fluent:damaged, obj:car], tp:1).
happens_at([event:fix, pers:sue, obj:car], tp:1).
neg(holds_at([fluent:damaged, obj:car], tp:4)).
end(model(f18)).



begin(model(f19)).
holds_at([fluent:located, pers:mary, loc:garage], tp:3).
holds_at([fluent:located, obj:car, loc:garage], tp:3).
neg(holds_at([fluent:broken, obj:car], tp:3)).
happens_at([event:drive, pers:mary, loc:garage, loc:store], tp:3).
holds_at([fluent:located, pers:mary, loc:store], tp:5).
holds_at([fluent:located, obj:car, loc:store], tp:5).
neg(holds_at([fluent:located, pers:mary, loc:garage], tp:5)).
neg(holds_at([fluent:located, obj:car, loc:garage], tp:5)).
end(model(f19)).

begin(model(f20)).
holds_at([fluent:located, pers:mary, loc:store], tp:4).
holds_at([fluent:located, obj:car, loc:store], tp:4).
neg(holds_at([fluent:broken, obj:car], tp:4)).
happens_at([event:drive, pers:mary, loc:store, loc:garage], tp:4).
neg(holds_at([fluent:located, pers:mary, loc:garage], tp:5)).
neg(holds_at([fluent:located, obj:car, loc:garage], tp:5)).
holds_at([fluent:located, pers:mary, loc:store], tp:5).
holds_at([fluent:located, obj:car, loc:store], tp:5).
end(model(f20)).

begin(model(f21)).
holds_at([fluent:located, pers:mary, loc:yard], tp:2).
holds_at([fluent:located, obj:car, loc:yard], tp:2).
neg(holds_at([fluent:broken, obj:car], tp:2)).
happens_at([event:drive, pers:mary, loc:yard, loc:store], tp:2).
holds_at([fluent:located, pers:mary, loc:store], tp:3).
holds_at([fluent:located, obj:car, loc:store], tp:3).
neg(holds_at([fluent:located, pers:mary, loc:yard], tp:3)).
neg(holds_at([fluent:located, obj:car, loc:yard], tp:3)).
end(model(f21)).

begin(model(f22)).
holds_at([fluent:located, pers:sue, loc:store], tp:3).
holds_at([fluent:located, obj:car, loc:store], tp:3).
neg(holds_at([fluent:broken, obj:car], tp:3)).
happens_at([event:drive, pers:sue, loc:store, loc:office], tp:3).
holds_at([fluent:located, pers:sue, loc:office], tp:5).
holds_at([fluent:located, obj:car, loc:office], tp:5).
neg(holds_at([fluent:located, pers:sue, loc:store], tp:5)).
neg(holds_at([fluent:located, obj:car, loc:store], tp:5)).
end(model(f22)).

begin(model(f23)).
holds_at([fluent:located, pers:sue, loc:office], tp:2).
holds_at([fluent:located, obj:car, loc:office], tp:2).
neg(holds_at([fluent:broken, obj:car], tp:2)).
happens_at([event:drive, pers:sue, loc:office, loc:garage], tp:2).
holds_at([fluent:located, pers:sue, loc:garage], tp:4).
holds_at([fluent:located, obj:car, loc:garage], tp:4).
neg(holds_at([fluent:located, pers:sue, loc:office], tp:4)).
neg(holds_at([fluent:located, obj:car, loc:office], tp:4)).
end(model(f23)).

begin(model(f24)).
holds_at([fluent:carrying, pers:mary, obj:book], tp:1).
happens_at([event:put_down, pers:mary, obj:book], tp:1).
neg(holds_at([fluent:carrying, pers:mary, obj:book], tp:5)).
end(model(f24)).

begin(model(f25)).
holds_at([fluent:carrying, pers:mary, obj:book], tp:0).
happens_at([event:put_down, pers:mary, obj:book], tp:0).
neg(holds_at([fluent:carrying, pers:mary, obj:book], tp:2)).
end(model(f25)).

begin(model(f26)).
holds_at([fluent:carrying, pers:mary, obj:book], tp:3).
happens_at([event:put_down, pers:mary, obj:book], tp:3).
neg(holds_at([fluent:carrying, pers:mary, obj:book], tp:5)).
end(model(f26)).

begin(model(f27)).
holds_at([fluent:carrying, pers:mary, obj:book], tp:1).
happens_at([event:put_down, pers:mary, obj:book], tp:1).
holds_at([fluent:carrying, pers:mary, obj:book], tp:2).
end(model(f27)).


begin(model(f28)).
holds_at([fluent:located, pers:mary, loc:bedroom], tp:1).
holds_at([fluent:located, obj:book, loc:bedroom], tp:1).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
holds_at([fluent:carrying, pers:mary, obj:book], tp:5).
happens_at([event:walk, pers:mary, loc:bedroom, loc:office], tp:5).
holds_at([fluent:located, obj:book, loc:office], tp:6).
neg(holds_at([fluent:located, obj:book, loc:bedroom], tp:6)).
end(model(f28)).

begin(model(f29)).
holds_at([fluent:located, pers:mary, loc:bedroom], tp:0).
holds_at([fluent:located, obj:book, loc:bedroom], tp:0).
happens_at([event:pick_up, pers:mary, obj:book], tp:0).
holds_at([fluent:carrying, pers:mary, obj:book], tp:2).
happens_at([event:walk, pers:mary, loc:bedroom, loc:bathroom], tp:2).
holds_at([fluent:located, obj:book, loc:bathroom], tp:6).
neg(holds_at([fluent:located, obj:book, loc:bedroom], tp:6)).
end(model(f29)).

begin(model(f30)).
holds_at([fluent:located, pers:mary, loc:office], tp:3).
holds_at([fluent:located, obj:book, loc:office], tp:3).
happens_at([event:pick_up, pers:mary, obj:book], tp:3).
holds_at([fluent:carrying, pers:mary, obj:book], tp:5).
happens_at([event:walk, pers:mary, loc:office, loc:bathroom], tp:5).
neg(holds_at([fluent:located, obj:book, loc:bathroom], tp:7)).
holds_at([fluent:located, obj:book, loc:office], tp:7).
end(model(f30)).

begin(model(f31)).
holds_at([fluent:located, pers:mary, loc:bathroom], tp:1).
holds_at([fluent:located, obj:book, loc:bathroom], tp:1).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
holds_at([fluent:carrying, pers:mary, obj:book], tp:2).
happens_at([event:walk, pers:mary, loc:bathroom, loc:garage], tp:2).
holds_at([fluent:located, obj:book, loc:garage], tp:3).
neg(holds_at([fluent:located, obj:book, loc:bathroom], tp:5)).
end(model(f31)).

begin(model(f32)).
holds_at([fluent:located, pers:mary, loc:garage], tp:1).
holds_at([fluent:located, obj:book, loc:garage], tp:1).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
neg(holds_at([fluent:carrying, pers:mary, obj:book], tp:4)).
happens_at([event:walk, pers:mary, loc:garage, loc:hallway], tp:4).
holds_at([fluent:located, obj:book, loc:hallway], tp:5).
neg(holds_at([fluent:located, obj:book, loc:garage], tp:5)).
end(model(f32)).

begin(model(f33)).
holds_at([fluent:located, pers:mary, loc:bedroom], tp:1).
holds_at([fluent:located, obj:book, loc:bedroom], tp:1).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
holds_at([fluent:carrying, pers:mary, obj:book], tp:5).
happens_at([event:drive, pers:mary, loc:bedroom, loc:office], tp:5).
holds_at([fluent:located, obj:book, loc:office], tp:6).
neg(holds_at([fluent:located, obj:book, loc:bedroom], tp:6)).
end(model(f33)).

begin(model(f34)).
holds_at([fluent:located, pers:mary, loc:bedroom], tp:0).
holds_at([fluent:located, obj:book, loc:bedroom], tp:0).
happens_at([event:pick_up, pers:mary, obj:book], tp:0).
holds_at([fluent:carrying, pers:mary, obj:book], tp:2).
happens_at([event:drive, pers:mary, loc:bedroom, loc:bathroom], tp:2).
holds_at([fluent:located, obj:book, loc:bathroom], tp:6).
neg(holds_at([fluent:located, obj:book, loc:bedroom], tp:6)).
end(model(f34)).

begin(model(f35)).
holds_at([fluent:located, pers:mary, loc:office], tp:3).
holds_at([fluent:located, obj:book, loc:office], tp:3).
happens_at([event:pick_up, pers:mary, obj:book], tp:3).
holds_at([fluent:carrying, pers:mary, obj:book], tp:5).
happens_at([event:drive, pers:mary, loc:office, loc:bathroom], tp:5).
neg(holds_at([fluent:located, obj:book, loc:bathroom], tp:7)).
holds_at([fluent:located, obj:book, loc:office], tp:7).
end(model(f35)).

begin(model(f36)).
holds_at([fluent:located, pers:mary, loc:bathroom], tp:1).
holds_at([fluent:located, obj:book, loc:bathroom], tp:1).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
holds_at([fluent:carrying, pers:mary, obj:book], tp:2).
happens_at([event:drive, pers:mary, loc:bathroom, loc:garage], tp:2).
holds_at([fluent:located, obj:book, loc:garage], tp:3).
neg(holds_at([fluent:located, obj:book, loc:bathroom], tp:3)).
end(model(f36)).

begin(model(f37)).
holds_at([fluent:located, pers:mary, loc:garage], tp:1).
holds_at([fluent:located, obj:book, loc:garage], tp:1).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
neg(holds_at([fluent:carrying, pers:mary, obj:book], tp:4)).
happens_at([event:drive, pers:mary, loc:garage, loc:hallway], tp:4).
holds_at([fluent:located, obj:book, loc:hallway], tp:5).
neg(holds_at([fluent:located, obj:book, loc:garage], tp:5)).
end(model(f37)).
% ----------------------------------------------------------------------
% Fold
% ----------------------------------------------------------------------

fold(train, [f1, f2, f3, f4, f5, f6, f7, f8, f9,f10,f11,f12,f13,f14,f15,f16,f17,f18,f19,f20,f21,f22,f23,f24,f25,f26,f27,f28,f29,f30,f31,f32,f33,f34,f35,f36,f37]).

% ----------------------------------------------------------------------
% Learn effect axioms
% ----------------------------------------------------------------------

learn_parameters(C) :-
    induce_par([train], C),
    numbervars(C, 0, _),
    write_clauses(C).

write_clauses([]) :-
    nl, nl.

write_clauses([C|Cs]) :-
    write(C),
    nl, nl,
    write_clauses(Cs).