% ======================================================================
% Tiny Probabilistic Event Calculus
%
% Authors:
%   Rolf Schwitter   (Macquarie University, Australia)
%   Fabrizio Riguzzi (University Ferrara, Italy)
%
% Date:
%   24th February 2017
% ======================================================================


% ----------------------------------------------------------------------
% Domain-independent axioms
% ----------------------------------------------------------------------

holds_at(F, tp:T) :-
  initially(F),
  \+ clipped(tp:0, F, tp:T).

holds_at(F, tp:T2) :-
  initiated_at(F, tp:T1),
  T1 < T2,
  \+ clipped(tp:T1, F, tp:T2).

clipped(tp:T1, F, tp:T3) :-
  terminated_at(F, tp:T2),
  T1 < T2, T2 < T3.




% ----------------------------------------------------------------------
% Initial situation
% ----------------------------------------------------------------------

initially([fluent:located, pers:bob, loc:garden]).


% ----------------------------------------------------------------------
% Noisy events
% ----------------------------------------------------------------------

happens_at([event:arrive, pers:bob, loc:kitchen], tp:5).
happens_at([event:arrive, pers:bob, loc:garage], tp:10).

% ----------------------------------------------------------------------
% Added events
% ----------------------------------------------------------------------

happens_at([event:pick_up, pers:bob, obj:bottle], tp:11).
happens_at([event:arrive, pers:bob, loc:kitchen], tp:12).
happens_at([event:put_down, pers:bob, obj:bottle], tp:13).
happens_at([event:arrive, pers:bob, loc: garage], tp:14).

% ----------------------------------------------------------------------
% Probabilistic effect axioms
%   - output of: learn_effect_axioms.pl
% ----------------------------------------------------------------------

initiated_at([fluent:located, pers:A, loc:B], tp:D):-
  happens_at([event:arrive, pers:A, loc:B], tp:D).

terminated_at([fluent:located, pers:A, loc:B], tp:E) :-
  happens_at([event:arrive, pers:A, loc:_], tp:E),
  location([loc: B]).


% ----------------------------------------------------------------------
% Indirect effects
%
% ----------------------------------------------------------------------

terminated_at([fluent:carrying, pers:A, obj:B], tp: C):-
    happens_at([event:put_down,pers:A,obj:B], tp: C).

initiated_at([fluent:located, obj: A, loc:B], tp:C) :-
    happens_at([event:arrive,pers:D, loc:B], tp:C),
    holds_at([fluent:carrying,pers:D, obj:A], tp:C).

initiated_at([fluent:carrying, pers:A, obj:B], tp:C):-
    happens_at([event:pick_up, pers:A,obj:B], tp: C).

% ----------------------------------------------------------------------
% Auxiliary facts to guarantee range restriction
% ----------------------------------------------------------------------

location([loc: garden]).
location([loc: garage]).
location([loc: kitchen]).




test_ec_pita([P1, P2, P3, P4, P5, P6, P7]) :-
  prob( holds_at([fluent:located, pers:bob, loc:garden],  tp:3),  P1),
  prob( holds_at([fluent:located, pers:bob, loc:garden],  tp:5),  P2),
  prob( holds_at([fluent:located, pers:bob, loc:garden],  tp:7),  P3),
  prob( holds_at([fluent:located, pers:bob, loc:kitchen], tp:7),  P4),
  prob( holds_at([fluent:located, pers:bob, loc:kitchen], tp:10), P5),
  prob( holds_at([fluent:located, pers:bob, loc:kitchen], tp:11), P6),
  prob( holds_at([fluent:located, pers:bob, loc:garage],  tp:12), P7).