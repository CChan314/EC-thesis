begin(model(f1)).
holds_at([fluent:located, pers:mary, loc:yard], tp:0).
happens_at([event:arrive, pers:mary, loc:kitchen], tp:3).
holds_at([fluent:located, pers:mary, loc:kitchen], tp:5).
end(model(f1)).

begin(model(f2)).
holds_at([fluent:located, pers:mary, loc:yard], tp:0).
happens_at([event:arrive, pers:mary, loc:garage], tp:4).
neg(holds_at([fluent:located, pers:mary, loc:garage], tp:5)).
end(model(f2)).

begin(model(f3)).
holds_at([fluent:located, pers:mary, loc:yard], tp:1).
happens_at([event:arrive, pers:mary, loc:kitchen], tp:2).
holds_at([fluent:located, pers:mary, loc:kitchen], tp:3).
end(model(f3)).

begin(model(f4)).
holds_at([fluent:located, pers:sue, loc:hallway], tp:1).
happens_at([event:arrive, pers:sue, loc:office], tp:3).
holds_at([fluent:located, pers:sue, loc:office], tp:5).
end(model(f4)).

begin(model(f5)).
holds_at([fluent:located, pers:sue, loc:bedroom], tp:1).
happens_at([event:arrive, pers:sue, loc:bathroom], tp:2).
holds_at([fluent:located, pers:sue, loc:bathroom], tp:4).
end(model(f5)).

begin(model(f6)).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
holds_at([fluent:carrying, pers:mary, obj:book], tp:5).
happens_at([event:put_down, pers:mary, obj:book], tp:7).
neg(holds_at([fluent:carrying, pers:mary, obj:book], tp:9)).
end(model(f6)).

begin(model(f7)).
happens_at([event:pick_up, pers:mary, obj:book], tp:0).
holds_at([fluent:carrying, pers:mary, obj:book], tp:2).
neg(happens_at([event:put_down, pers:mary, obj:book], tp:3)).
neg(holds_at([fluent:carrying, pers:mary, obj:book], tp:5)).
end(model(f7)).

begin(model(f8)).
happens_at([event:pick_up, pers:mary, obj:book], tp:3).
holds_at([fluent:carrying, pers:mary, obj:book], tp:5).
happens_at([event:put_down, pers:mary, obj:book], tp:7).
holds_at([fluent:carrying, pers:mary, obj:book], tp:9).
end(model(f8)).

begin(model(f9)).
happens_at([event:pick_up, pers:alice, obj:book], tp:1).
holds_at([fluent:carrying, pers:alice, obj:book], tp:2).
end(model(f9)).

begin(model(f10)).
happens_at([event:pick_up, pers:bob, obj:book], tp:1).
holds_at([fluent:carrying, pers:bob, obj:book], tp:4).
happens_at([event:put_down, pers:bob, obj:book], tp:7).
neg(holds_at([fluent:carrying, pers:bob, obj:book], tp:8)).
end(model(f10)).

begin(model(f11)).
happens_at([event:pick_up, pers:bob, obj:screwdriver], tp:1).
holds_at([fluent:carrying, pers:bob, obj:screwdriver], tp:2).
end(model(f11)).

begin(model(f12)).
happens_at([event:pick_up, pers:mary, obj:screwdriver], tp:1).
neg(holds_at([fluent:carrying, pers:mary, obj:screwdriver], tp:2)).
end(model(f12)).

begin(model(f13)).
happens_at([event:fix, pers:alice, obj:car], tp:1).
neg(holds_at([fluent:damaged, obj:car], tp:5)).
end(model(f13)).

begin(model(f14)).
happens_at([event:fix, pers:sue, obj:car], tp:1).
neg(holds_at([fluent:damaged, obj:car], tp:4)).
end(model(f14)).

begin(model(f15)).
happens_at([event:fix, pers:bob, obj:car], tp:1).
holds_at([fluent:damaged, obj:car], tp:3).
end(model(f15)).

begin(model(f16)).
happens_at([event:fix, pers:eve, obj:car], tp:1).
neg(holds_at([fluent:damaged, obj:car], tp:4)).
end(model(f16)).

begin(model(f17)).
happens_at([event:break, obj:car], tp:1).
neg(holds_at([fluent:broken, obj:car], tp:2)).
end(model(f17)).

begin(model(f18)).
happens_at([event:break, obj:car], tp:1).
holds_at([fluent:broken, obj:car], tp:3).
end(model(f18)).

begin(model(f19)).
happens_at([event:break, obj:car], tp:1).
holds_at([fluent:broken, obj:car], tp:5).
end(model(f19)).

begin(model(f20)).
happens_at([event:break, obj:car], tp:1).
holds_at([fluent:broken, obj:car], tp:4).
end(model(f20)).