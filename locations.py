def addLocations(locations):
    rule = "\n :- begin_bg. \n"
    for location in locations:
        rule += "location([loc: "+location+"]).\n"
    rule += ":- end_bg. \n"
    with open('learn_effect_axioms.pl','a') as myfile:
        myfile.write(rule)

addLocations(['bathroom','bedroom','garage','hallway','kitchen','office','yard'])