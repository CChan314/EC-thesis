:- begin_bg.

location([loc: bathroom]).
location([loc: bedroom]).
location([loc: garage]).
location([loc: hallway]).
location([loc: kitchen]).
location([loc: office]).
location([loc: yard]).

:- end_bg.