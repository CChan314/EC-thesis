% ======================================================================
% Learning Probabilistic Effect Axioms
%
% Authors:
%   Christopher Chan (Macquarie University, Sydney)
%   Rolf Schwitter   (Macquarie University, Sydney)
%
% Date:
%   10th October 2017
% ======================================================================


/** <examples>

?- learn_parameters(C).

*/
:- use_module(library(slipcover)).

:- if(current_predicate(use_rendering/1)).
:- use_rendering(c3).
:- use_rendering(lpad).
:- endif.


:- sc.

:- set_sc(max_var, 4).
:- set_sc(verbosity, 3).
:- set_sc(megaex_bottom, 5).
:- set_sc(depth_bound, false).
:- set_sc(neg_ex, given).


% ----------------------------------------------------------------------
% Language bias: initiated_at/2
% ----------------------------------------------------------------------

output(initiated_at/3).
input(happens_at/2).
input('<'/2).

determination(initiated_at/3, happens_at/2).
determination(initiated_at/3, '<'/2).

% ----------------------------------------------------------------------
% Language bias: terminated_at/2
% ----------------------------------------------------------------------

output(terminated_at/2).
input(happens_at/2).

determination(terminated_at/2, happens_at/2).

% ----------------------------------------------------------------------
% Background information
% ----------------------------------------------------------------------
:- begin_in.

initiated_at([fluent:located, pers:P, loc:L2], tp:T1, tp:T2):0.5 :-
  happens_at([event:walk, pers:P, loc:L1, loc:L2], tp:T1),
  T1 < T2, \+ L1 = L2.
   
:- end_in.

:- begin_bg.

location([loc: bathroom]).
location([loc: bedroom]).
location([loc: garage]).
location([loc: hallway]).
location([loc: kitchen]).
location([loc: office]).
location([loc: yard]).
location([loc: store]).

:- end_bg.

% Example generation:
% ----------------------------------------------------------------------
% Positive effect axiom
% ----------------------------------------------------------------------
% 

% walks + located at and drive
initiated_at(ID, [fluent:F2, pers:P, loc:L2], tp:T2, tp:T4) :-
  holds_at(ID, [fluent:_F1, pers:P, loc:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2, tp:T4),
  T1 < T2,
  holds_at(ID, [fluent:F2, pers:P, loc:L2], tp:T3),
  T2 < T3.
  
 neg(initiated_at(ID, [fluent:F2, pers:P, loc:L2], tp:T2, tp:T4)) :-
  holds_at(ID, [fluent:_F1, pers:P, loc:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2, tp:T4),
  T1 < T2,
  neg(holds_at(ID, [fluent:F2, pers:P, loc:L2], tp:T3)),
  T2 < T3.


% Example generation:
% ----------------------------------------------------------------------
% Negative effect axiom
% ----------------------------------------------------------------------

% walk > located
terminated_at(ID, [fluent:F1, pers:P, loc:L1], tp:T2) :-
  holds_at(ID, [fluent:F1, pers:P, loc:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
  T1 < T2,
  holds_at(ID, [fluent:_F2, pers:P, loc:L2], tp:T3),
  T2 < T3.

neg(terminated_at(ID, [fluent:F1, pers:P, loc:L1], tp:T2)) :-
  holds_at(ID, [fluent:F1, pers:P, loc:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
  T1 < T2,
  neg(holds_at(ID, [fluent:_F2, pers:P, loc:L2], tp:T3)),
  T2 < T3. 

% ----------------------------------------------------------------------
% Example interpretations
% ----------------------------------------------------------------------

begin(model(f5)).
holds_at([fluent:located, pers:mary, loc:yard], tp:0).
happens_at([event:walk, pers:mary, loc:yard, loc:kitchen], tp:3, tp:4).
holds_at([fluent:located, pers:mary, loc:kitchen], tp:5).
end(model(f5)).

begin(model(f6)).
holds_at([fluent:located, pers:mary, loc:yard], tp:0).
happens_at([event:walk, pers:mary, loc:yard, loc:garage], tp:4, tp:5).
neg(holds_at([fluent:located, pers:mary, loc:garage], tp:5)).
end(model(f6)).

begin(model(f7)).
holds_at([fluent:located, pers:mary, loc:yard], tp:1).
happens_at([event:walk, pers:mary, loc:yard, loc:kitchen], tp:2, tp:3).
holds_at([fluent:located, pers:mary, loc:kitchen], tp:3).
end(model(f7)).

begin(model(f8)).
holds_at([fluent:located, pers:sue, loc:hallway], tp:1).
happens_at([event:walk, pers:sue, loc:hallway, loc:office], tp:3, tp:4).
holds_at([fluent:located, pers:sue, loc:office], tp:5).
end(model(f8)).

begin(model(f9)).
holds_at([fluent:located, pers:sue, loc:bedroom], tp:1).
happens_at([event:walk, pers:sue, loc:bedroom,  loc:bathroom], tp:2,tp:3).
holds_at([fluent:located, pers:sue, loc:bathroom], tp:4).
end(model(f9)).

% ----------------------------------------------------------------------
% Fold
% ----------------------------------------------------------------------

fold(train, [ f5, f6, f7, f8, f9]).

% ----------------------------------------------------------------------
% Learn effect axioms
% ----------------------------------------------------------------------

learn_parameters(C) :-
    induce_par([train], C),
    numbervars(C, 0, _),
    write_clauses(C).

write_clauses([]) :-
    nl, nl.

write_clauses([C|Cs]) :-
    write(C),
    nl, nl,
    write_clauses(Cs).
