% ======================================================================
% Carrying Tool
%
% Authors:
%   Christopher Chan
%
% Date:
%   14th September 2017
% ======================================================================

/** <examples>

?- test_ec_pita(Probs).


*/

:- use_module(library(pita)).

:- pita.



% ----------------------------------------------------------------------

:- begin_lpad.


% ----------------------------------------------------------------------
% Domain-independent axioms
% ----------------------------------------------------------------------

holds_at(F, tp:T) :-
  initially(F),
  \+ clipped(tp:0, F, tp:T).

holds_at(F, tp:T2) :-
  initiated_at(F, tp:T1),
  T1 < T2,
  \+ clipped(tp:T1, F, tp:T2).

clipped(tp:T1, F, tp:T3) :-
  terminated_at(F, tp:T2),
  T1 < T2, T2 < T3.

declipped(tp:T1,F,tp:T3) :-
    initiated_at(F, tp:T2),
    T1 < T2, T2 < T3.

% holds_at(F, tp:T1):0;'':1 :-
%	terminated_at(F, T:T2),
%    T2 < T1,

notholds_at(F, tp:T2) :-
	terminated_at(F, tp:T1),
    T1 < T2,
    \+ declipped(tp:T1, F, tp:T2).

%released_at(F, tp: T1) :-


% ----------------------------------------------------------------------
% Initial situation
% ----------------------------------------------------------------------

initially([fluent:located, pers:bob, loc:garden]).
initially([fluent:located, obj:tool, loc:kitchen]).


% ----------------------------------------------------------------------
% Events
% ----------------------------------------------------------------------

happens_at([event:walks, pers:bob, loc:garden, loc:kitchen], tp:1).
happens_at([event:pick_up, pers:bob, obj:tool], tp:2).
happens_at([event:walks, pers:bob, loc:kitchen, loc:garage], tp:3).
happens_at([event:put_down, pers:bob, obj:tool], tp:4).

% ----------------------------------------------------------------------
% Effect axioms
% ----------------------------------------------------------------------

%when a person walks into another room they are located in that room
initiated_at([fluent:located, pers:P, loc:L], tp:T):-
    happens_at([event:walks, pers:P, loc:_, loc:L], tp:T).

%when a person carries an object and walks to another room that object is now in that room
initiated_at([fluent:located, obj:O, loc:L], tp:T):-
    happens_at([event:walks, pers:P, loc:_, loc:L], tp:T),
    holds_at([fluent:carrying, pers:P, obj:O], tp:T).

%when a person picks up an object they are carrying that object
initiated_at([fluent:carrying, pers:P, obj:O], tp:T):-
    happens_at([event:pick_up, pers:P, obj:O], tp:T).

%when a person walks into another room they are no longer located in the original room
terminated_at([fluent:located, pers:P, loc:L], tp:T):-
    happens_at([event:walks, pers:P, loc:L, loc:_], tp:T),
    location([loc: L]).

%when someone walks into another room carrying an object the object is not longer located in the room
terminated_at([fluent:located, obj:O, loc:L], tp:T):-
    happens_at([event:walks, pers:P, loc:L, loc:_], tp:T),
    holds_at([fluent:carrying, pers:P, obj:O], tp:T),
    location([loc: L]).

%when a person puts down an object they are no longer carrying it
terminated_at([fluent:carrying, pers:P, obj:O], tp:T):-
    happens_at([event:put_down, pers:P, obj:O], tp:T).



% ----------------------------------------------------------------------
% Auxiliary facts to guarantee range restriction
% ----------------------------------------------------------------------

location([loc: garden]).
location([loc: garage]).
location([loc: kitchen]).



:- end_lpad.
%
%
test_ec_pita([P1, P2, P3, P4]) :-
  prob( holds_at([fluent:located, pers:bob, loc:garage],  tp:7),  P1),
  prob( holds_at([fluent:located, obj:tool, loc:garage],  tp:7),  P2),
  prob( holds_at([fluent:carrying, pers:bob, obj:tool],  tp:7),  P3),
  prob( notholds_at([fluent:carrying, pers:bob, obj:tool],  tp:7),  P4).