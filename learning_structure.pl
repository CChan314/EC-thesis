% ======================================================================
% Learning Probabilistic Effect Axioms
%
% Authors:
%   Christopher Chan (Macquarie Universtiy, Sydney)
%   Rolf Schwitter   (Macquarie University, Sydney)
%
% Date:
%   14th October 2017
% ======================================================================


/** <examples>

?- learn_effect_axioms(C).

*/
:- use_module(library(slipcover)).

:- if(current_predicate(use_rendering/1)).
:- use_rendering(c3).
:- use_rendering(lpad).
:- endif.


:- sc.

:- set_sc(max_var, 4).
:- set_sc(verbosity, 3).
:- set_sc(megaex_bottom, 23).
:- set_sc(depth_bound, false).
:- set_sc(neg_ex, given).


% ----------------------------------------------------------------------
% Language bias: initiated_at/2
% ----------------------------------------------------------------------
/*
% break > broken
output(initiated_at/2).
input(happens_at/2).

determination(initiated_at/2, happens_at/2).

modeh(*, initiated_at([fluent: -#fl, obj: +obj], tp: +tp)).
modeb(*, happens_at([event: -#ev, obj: +obj],  tp: +tp)).

% walk > located
modeh(*, initiated_at([fluent: -#fl, pers: +pers, loc: +loc1], tp: +tp)).
modeb(*, happens_at([event: -#ev, pers: +pers, loc: -loc, loc: +loc1],  tp: +tp)).

% pick_up > carrying
modeh(*, initiated_at([fluent: -#fl, pers: +pers, obj: +obj], tp: +tp)).
modeb(*, happens_at([event: -#ev, pers: +pers, obj: +obj],  tp: +tp)).
*/
% ----------------------------------------------------------------------
% Language bias: terminated_at/2
% ----------------------------------------------------------------------

output(terminated_at/2).
input(happens_at/2).

determination(terminated_at/2, happens_at/2).

% walk + drive > located
modeh(*, terminated_at([fluent: -#fl, pers: +pers, loc: +loc1], tp: +tp)).
modeb(*, happens_at([event: -#ev, pers: +pers, loc: +loc1, loc: -loc2], tp: +tp)).

% fix > damaged
modeh(*, terminated_at([fluent: -#fl, obj: +obj1], tp: +tp)).
modeb(*, happens_at([event: -#ev, pers: -pers, obj: +obj1], tp: +tp)).

% pick_up > carrying
modeh(*, terminated_at([fluent: -#fl, pers: +pers, obj: +obj], tp: +tp)).
modeb(*, happens_at([event: -#ev, pers: +pers, obj: +obj],  tp: +tp)).

% ----------------------------------------------------------------------
% Background information
% ----------------------------------------------------------------------
:- begin_in.
/*
initiated_at([fluent:broken,obj:A],tp:B):0.5 :-
   happens_at([event:break,obj:A],tp:B).
 
initiated_at([fluent:located,pers:A,loc:B],tp:C):0.5 :-
   happens_at([event:walk,pers:A,loc:_D,loc:B],tp:C).

initiated_at([fluent:located,pers:A,loc:B],tp:C):0.5 :-
   happens_at([event:drive,pers:A,loc:_D,loc:B],tp:C).
 
initiated_at([fluent:carrying,pers:A,obj:B],tp:C):0.5 :-
   happens_at([event:pick_up,pers:A,obj:B],tp:C).

terminated_at([fluent:damaged, obj:A], tp:T1):0.5:-
    happens_at([event:fix, pers:_B, obj:A], tp:T1).

terminated_at([fluent:located,pers:A,loc:B],tp:C):0.5 :-
   happens_at([event:walk,pers:A,loc:B,loc:_D],tp:C).

terminated_at([fluent:located,pers:A,loc:B],tp:C):0.5 :-
   happens_at([event:drive,pers:A,loc:B,loc:_D],tp:C).

terminated_at([fluent:carrying,pers:A,obj:B],tp:C):0.5 :-
   happens_at([event:put_down,pers:A,obj:B],tp:C).
*/
:- end_in.

:- begin_bg.

location([loc: bathroom]).
location([loc: bedroom]).
location([loc: garage]).
location([loc: hallway]).
location([loc: kitchen]).
location([loc: office]).
location([loc: yard]).
location([loc: store]).
:- end_bg.

% Example generation:
% ----------------------------------------------------------------------
% Positive effect axiom
% ----------------------------------------------------------------------
% 

% walks + located at and drive
initiated_at(ID, [fluent:F2, pers:P, loc:L2], tp:T2) :-
  holds_at(ID, [fluent:_F1, pers:P, loc:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
  T1 < T2,
  holds_at(ID, [fluent:F2, pers:P, loc:L2], tp:T3),
  T2 < T3.

neg(initiated_at(ID, [fluent:F2, pers:P, loc:L2], tp:T2)) :-
  holds_at(ID, [fluent:_F1, pers:P, loc:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, loc:L1,  loc:L2], tp:T2),
  T1 < T2,
  neg(holds_at(ID, [fluent:F2, pers:P, loc:L2], tp:T3)),
  T2 < T3.

% break > broken
initiated_at(ID, [fluent:F1, obj:O], tp:T2) :-
  happens_at(ID, [event:_E, obj:O], tp:T2),
  holds_at(ID, [fluent:F1, obj:O], tp:T3),
  T2 < T3.

neg(initiated_at(ID, [fluent:F1, obj:O], tp:T2)) :-
  happens_at(ID, [event:_E, obj:O], tp:T2),
  neg(holds_at(ID, [fluent:F1, obj:O], tp:T3)),
  T2 < T3.

% pick_up > carrying
initiated_at(ID, [fluent:F2, pers:P, obj:L2], tp:T2) :-
  happens_at(ID, [event:_E, pers:P, obj:L2], tp:T2),
  holds_at(ID, [fluent:F2, pers:P, obj:L2], tp:T3),
  T2 < T3.

neg(initiated_at(ID, [fluent:F2, pers:P, obj:_L], tp:T2)) :-
  happens_at(ID, [event:_E, pers:P, obj:L2], tp:T2),
  neg(holds_at(ID, [fluent:F2, pers:P, obj:L2], tp:T3)),
  T2 < T3.


% Example generation:
% ----------------------------------------------------------------------
% Negative effect axiom
% ----------------------------------------------------------------------

% walk > located
terminated_at(ID, [fluent:F1, pers:P, loc:L1], tp:T2) :-
  holds_at(ID, [fluent:F1, pers:P, loc:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
  T1 < T2,
  holds_at(ID, [fluent:_F2, pers:P, loc:L2], tp:T3),
  T2 < T3.

neg(terminated_at(ID, [fluent:F1, pers:P, loc:L1], tp:T2)) :-
  holds_at(ID, [fluent:F1, pers:P, loc:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, loc:L1, loc:L2], tp:T2),
  T1 < T2,
  neg(holds_at(ID, [fluent:_F2, pers:P, loc:L2], tp:T3)),
  T2 < T3. 

terminated_at(ID, [fluent:F1, pers:P, obj:L1], tp:T2) :-
  holds_at(ID, [fluent:F1, pers:P, obj:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, obj:L1], tp:T2),
  T1 < T2,
  neg(holds_at(ID, [fluent:_F2, pers:P, obj:L1], tp:T3)),
  T2 < T3.

neg(terminated_at(ID, [fluent:F1, pers:P, obj:L1], tp:T2)) :-
  holds_at(ID, [fluent:F1, pers:P, obj:L1], tp:T1),
  happens_at(ID, [event:_E, pers:P, obj:L1], tp:T2),
  T1 < T2,
  holds_at(ID, [fluent:_F2, pers:P, obj:L1], tp:T3),
  T2 < T3.

% fix > damaged
terminated_at(ID, [fluent:F1, obj:L1], tp:T2) :-
    holds_at(ID, [fluent:F1, obj:L1], tp:T1),
  happens_at(ID, [event:_E, pers:_P, obj:L1], tp:T2),
    T1 < T2,
  neg(holds_at(ID, [fluent:F1, obj:L1], tp:T3)),
  T2 < T3.

neg(terminated_at(ID, [fluent:F1, obj:L1], tp:T2)) :-
        holds_at(ID, [fluent:F1, obj:L1], tp:T1),
  happens_at(ID, [event:_E, pers:_P, obj:L1], tp:T2),
        T1 < T2,
  holds_at(ID, [fluent:F1, obj:L1], tp:T3),
  T2 < T3.



% ----------------------------------------------------------------------
% Example interpretations
% ----------------------------------------------------------------------

begin(model(f1)).
happens_at([event:break, obj:car], tp:1).
neg(holds_at([fluent:broken, obj:car], tp:2)).
end(model(f1)).

begin(model(f2)).
happens_at([event:break, obj:car], tp:1).
holds_at([fluent:broken, obj:car], tp:3).
end(model(f2)).

begin(model(f3)).
happens_at([event:break, obj:car], tp:1).
holds_at([fluent:broken, obj:car], tp:5).
end(model(f3)).

begin(model(f4)).
happens_at([event:break, obj:car], tp:1).
holds_at([fluent:broken, obj:car], tp:4).
end(model(f4)).

begin(model(f5)).
holds_at([fluent:located, pers:mary, loc:yard], tp:0).
happens_at([event:walk, pers:mary, loc:yard, loc:kitchen], tp:3).
holds_at([fluent:located, pers:mary, loc:kitchen], tp:5).
end(model(f5)).

begin(model(f6)).
holds_at([fluent:located, pers:mary, loc:yard], tp:0).
happens_at([event:walk, pers:mary, loc:yard, loc:garage], tp:4).
neg(holds_at([fluent:located, pers:mary, loc:garage], tp:5)).
end(model(f6)).

begin(model(f7)).
holds_at([fluent:located, pers:mary, loc:yard], tp:1).
happens_at([event:walk, pers:mary, loc:yard, loc:kitchen], tp:2).
holds_at([fluent:located, pers:mary, loc:kitchen], tp:3).
end(model(f7)).

begin(model(f8)).
holds_at([fluent:located, pers:sue, loc:hallway], tp:1).
happens_at([event:walk, pers:sue, loc:hallway, loc:office], tp:3).
holds_at([fluent:located, pers:sue, loc:office], tp:5).
end(model(f8)).

begin(model(f9)).
holds_at([fluent:located, pers:sue, loc:bedroom], tp:1).
happens_at([event:walk, pers:sue, loc:bedroom,  loc:bathroom], tp:2).
holds_at([fluent:located, pers:sue, loc:bathroom], tp:4).
end(model(f9)).

begin(model(f10)).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
holds_at([fluent:carrying, pers:mary, obj:book], tp:5).
end(model(f10)).

begin(model(f11)).
happens_at([event:pick_up, pers:mary, obj:book], tp:0).
holds_at([fluent:carrying, pers:mary, obj:book], tp:2).
end(model(f11)).

begin(model(f12)).
happens_at([event:pick_up, pers:mary, obj:book], tp:3).
holds_at([fluent:carrying, pers:mary, obj:book], tp:5).
end(model(f12)).

begin(model(f13)).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
holds_at([fluent:carrying, pers:mary, obj:book], tp:2).
end(model(f13)).

begin(model(f14)).
happens_at([event:pick_up, pers:mary, obj:book], tp:1).
holds_at([fluent:carrying, pers:mary, obj:book], tp:4).
happens_at([event:put_down, pers:mary, obj:book], tp:7).
neg(holds_at([fluent:carrying, pers:mary, obj:book], tp:8)).
end(model(f14)).

begin(model(f15)).
holds_at([fluent:damaged, obj:car], tp:0).
happens_at([event:fix, pers:bob, obj:car], tp:1).
holds_at([fluent:damaged, obj:car], tp:3).
end(model(f15)).

begin(model(f16)).
holds_at([fluent:damaged, obj:car], tp:0).
happens_at([event:fix, pers:eve, obj:car], tp:1).
neg(holds_at([fluent:damaged, obj:car], tp:4)).
end(model(f16)).

begin(model(f17)).
holds_at([fluent:damaged, obj:car], tp:0).
happens_at([event:fix, pers:alice, obj:car], tp:1).
neg(holds_at([fluent:damaged, obj:car], tp:5)).
end(model(f17)).

begin(model(f18)).
holds_at([fluent:damaged, obj:car], tp:0).
happens_at([event:fix, pers:sue, obj:car], tp:1).
neg(holds_at([fluent:damaged, obj:car], tp:4)).
end(model(f18)).

begin(model(f19)).
holds_at([fluent:located, pers:mary, loc:garage], tp:0).
happens_at([event:drive, pers:mary, loc:garage, loc:store], tp:3).
holds_at([fluent:located, pers:mary, loc:store], tp:5).
end(model(f19)).

begin(model(f20)).
holds_at([fluent:located, pers:mary, loc:store], tp:0).
happens_at([event:drive, pers:mary, loc:store, loc:garage], tp:4).
neg(holds_at([fluent:located, pers:mary, loc:garage], tp:5)).
end(model(f20)).

begin(model(f21)).
holds_at([fluent:located, pers:mary, loc:yard], tp:1).
happens_at([event:drive, pers:mary, loc:yard, loc:store], tp:2).
holds_at([fluent:located, pers:mary, loc:store], tp:3).
end(model(f21)).

begin(model(f22)).
holds_at([fluent:located, pers:sue, loc:store], tp:1).
happens_at([event:drive, pers:sue, loc:store, loc:office], tp:3).
holds_at([fluent:located, pers:sue, loc:office], tp:5).
end(model(f22)).

begin(model(f23)).
holds_at([fluent:located, pers:sue, loc:office], tp:1).
happens_at([event:drive, pers:sue, loc:office, loc:garage], tp:2).
neg(holds_at([fluent:located, pers:sue, loc:office], tp:4)).
end(model(f23)).



% ----------------------------------------------------------------------
% Fold
% ----------------------------------------------------------------------
fold(train, [f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14, f15, f16, f17, f18,f19,f20,f21,f22,f23]).
% ----------------------------------------------------------------------
% Learn effect axioms
% ----------------------------------------------------------------------

learn_effect_axioms(C) :-
  induce([train], C).