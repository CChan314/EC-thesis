% ======================================================================
% Probabilistic Event Calculus: Indirect Effects
%
% Author:
%   Christopher Chan (Macquarie University, Australia)
%   Rolf Schwitter   (Macquarie University, Australia)
%
% Date:
%   23rd September 2017
% ======================================================================

:- style_check([-singleton, -discontiguous]).


% ----------------------------------------------------------------------

:- use_module(library(pita)).

:- pita.

:- begin_lpad.


% ----------------------------------------------------------------------
% Domain-independent axioms
% ----------------------------------------------------------------------

holds_at(Fluent, tp:T) :-
  initially(Fluent),
  \+ clipped(tp:0, Fluent, tp:T).

holds_at(Fluent, tp:T2) :-
  initiated_at(Fluent, tp:T1, tp:T2),     % Added tp:T2
  \+ clipped(tp:T1, Fluent, tp:T2).

clipped(tp:T1, Fluent, tp:T3) :-
  terminated_at(Fluent, tp:T2, tp:T3),    % Added tp:T3
  T1 < T2.                           
                                     
neg(holds_at(Fluent, tp:T2)) :-
   \+ holds_at(Fluent, tp:T2).


% ----------------------------------------------------------------------
% Initial situation
% ----------------------------------------------------------------------


%Bob wants to go to the store to buy some groceries, but first he needs to fix his car. Bob is very forgetful and
%sometimes he forgets about tasks he needs to complete. Will he be able to get to the store and back with all his shopping?

%At first Bob is in the garden and he needs to go to the store.
%The car is damaged, but can still run as it is not completely broken yet
%Bob needs to fix the car before he leaves otherwise it could break completely during the trip
initially([fluent:located, pers:bob, loc:garden]).
initially([fluent:located, obj:screwdriver, loc:kitchen]).
initially([fluent:located, obj:car, loc:garage]).
initially([fluent:located, obj:orange, loc:store]).
initially([fluent:located, obj:apple, loc:store]).
initially([fluent:damaged, obj:car]).

% ----------------------------------------------------------------------
% Events
% ----------------------------------------------------------------------

%Bob remembers that he has to go to the store and leaves the garden
happens_at([event:walk, pers:bob, loc:garden, loc:kitchen], tp:1).

% He picks up his screwdriver from the kitchen
happens_at([event:pick_up, pers:bob, obj:screwdriver], tp:3).

%Bob walks into the garage
happens_at([event:walk, pers:bob, loc:kitchen, loc:garage], tp:5).

%bob might forget to fix the car before he leaves and then puts down the screwdriver
%bob will only be able to fix the car if he is still holding the screwdriver
happens_at([event:put_down, pers:bob, obj:screwdriver], tp:6):0.2 ;
happens_at([event:fix, pers:bob, obj:car], tp:7):0.8.

%if bob is still carrying the screwdriver then he will put it down
happens_at([event:put_down, pers:bob, obj:screwdriver], tp:8).

%Bob drives to the store so he can get his groceries
happens_at([event:drive, pers:bob, loc:garage, loc:store], tp:9).

% Bob buys the apple
happens_at([event:pick_up, pers:bob, obj:apple], tp:11).

% Bob wants to buy an orange as well but may forget
happens_at([event:pick_up, pers:bob, obj:orange], tp:12):0.6.

%Bob turns on his car to leave the store, but if he didn't fix his car earlier
%it may not start and Bob will be stuck at the store
happens_at([event:break, obj:car], tp:13):0.2.

% If the car is running Bob goes home, with his apple and maybe his orange
happens_at([event:drive, pers:bob, loc:store, loc:garage], tp:14).


% ----------------------------------------------------------------------
% Effect axioms
% ----------------------------------------------------------------------

% 6.1
initiated_at([fluent:located,pers:A,loc:B],tp:C,tp:D):0.8 :-
   happens_at([event:walk,pers:A,loc:E,loc:B],tp:C),
   C<D,
   \+E=B.

% 6.2
initiated_at([fluent:located,pers:A,loc:B],tp:C,tp:D):0.8 :-
   happens_at([event:drive,pers:A,loc:E,loc:B],tp:C),
   C<D,
   \+E=B,
   \+holds_at([fluent:broken,obj:F],tp:C),
   holds_at([fluent:located,pers:A,loc:E],tp:C),
   holds_at([fluent:located,obj:F,loc:E],tp:C).

% 6.3
 initiated_at([fluent:broken,obj:A],tp:B,tp:C):0.75 :-
   happens_at([event:break,obj:A],tp:B),
   B<C,
   holds_at([fluent:damaged,obj:A],tp:B).
        
% 6.4
 initiated_at([fluent:located,obj:A,loc:B],tp:C,tp:D):0.8 :-
   happens_at([event:drive,pers:E,loc:F,loc:B],tp:C),
   C<D,
   \+F=B,
   \+holds_at([fluent:broken,obj:A],tp:C),
   holds_at([fluent:located,pers:E,loc:F],tp:C),
   holds_at([fluent:located,obj:A,loc:F],tp:C).

% 6.5
 initiated_at([fluent:located,obj:A,loc:B],tp:C,tp:D):0.75 :-
   happens_at([event:drive,pers:E,loc:F,loc:B],tp:C),
   C<D,
   \+F=B,
   holds_at([fluent:carrying,pers:E,obj:A],tp:C).

% 6.6
 initiated_at([fluent:carrying,pers:A,obj:B],tp:C,tp:D):0.8 :-
   happens_at([event:pick_up,pers:A,obj:B],tp:C),
   C<D,
   holds_at([fluent:located,pers:A,loc:E],tp:C),
   holds_at([fluent:located,obj:B,loc:E],tp:C).

% 6.7
 initiated_at([fluent:located,obj:A,loc:B],tp:C,tp:D):0.75 :-
   happens_at([event:walk,pers:E,loc:F,loc:B],tp:C),
   C<D,
   \+F=B,
   holds_at([fluent:carrying,pers:E,obj:A],tp:C).

 %--------------------------------------------------------------------------------
  
% 6.8
 terminated_at([fluent:located,pers:A,loc:B],tp:C,tp:D):0.8 :-
   happens_at([event:walk,pers:A,loc:B,loc:E],tp:C),
   C<D,
   \+B=E.
 

% 6.9
 terminated_at([fluent:carrying,pers:A,obj:B],tp:C,tp:D):0.75 :-
   happens_at([event:put_down,pers:A,obj:B],tp:C),
      C<D,
   holds_at([fluent:carrying,pers:A,obj:B],tp:C).

% 6.10
 terminated_at([fluent:located,obj:A,loc:B],tp:C,tp:D):0.25 :-
   happens_at([event:walk,pers:E,loc:B,loc:F],tp:C),
   C<D,
   \+B=F,
   holds_at([fluent:carrying,pers:E,obj:A],tp:C).
 

% 6.11
 terminated_at([fluent:located,pers:A,loc:B],tp:C,tp:D):0.8 :-
   happens_at([event:drive,pers:A,loc:B,loc:E],tp:C),
   C<D,
   \+B=E,
   \+holds_at([fluent:broken,obj:F],tp:C),
   holds_at([fluent:located,pers:A,loc:B],tp:C),
   holds_at([fluent:located,obj:F,loc:B],tp:C).

% 6.12
 terminated_at([fluent:located,obj:A,loc:B],tp:C,tp:D):0.8 :-
   happens_at([event:drive,pers:E,loc:B,loc:F],tp:C),
   C<D,
   \+B=F,
   \+holds_at([fluent:broken,obj:A],tp:C),
   holds_at([fluent:located,pers:E,loc:B],tp:C),
   holds_at([fluent:located,obj:A,loc:B],tp:C).

% 6.13
terminated_at([fluent:located,obj:A,loc:B],tp:C,tp:D):0.25 :-
   happens_at([event:drive,pers:E,loc:B,loc:F],tp:C),
   C<D,
   \+B=F,
   holds_at([fluent:carrying,pers:E,obj:A],tp:C).

% 6.14
terminated_at([fluent:damaged, obj:A], tp:B, tp:C) :-
    happens_at([event:fix, pers:D, obj:A], tp:B),
    B < C,
  	holds_at([fluent:located, pers:D, loc:E], tp:B),
 	holds_at([fluent:located, obj:B, loc:E], tp:B),
	holds_at([fluent:damaged, obj:B], tp:B).

:- end_lpad.


% ----------------------------------------------------------------------
% Tests
% ----------------------------------------------------------------------

test1 :-
  findall(holds_at([fluent:holding, pers:bob, obj:screwdriver], tp:T):P,
       ( member(T, [0, 1, 2, 3, 4, 5, 6, 7, 8]),
         prob( holds_at([fluent:holding, pers:bob, obj:screwdriver], tp:T), P) ),
          Result),
  print_result(Result).

test1n :-
  findall(neg(holds_at([fluent:holding, pers:bob, obj:screwdriver], tp:T)):P,
      ( member(T, [0, 1, 2, 3, 4, 5, 6, 7, 8]),
            prob( neg(holds_at([fluent:holding, pers:bob, obj:screwdriver], tp:T)), P) ),
          Result),
  print_result(Result).


% ---------------
   
test2 :-
  findall(holds_at([fluent:located, pers:bob, loc:garden], tp:T):P,
      ( member(T, [0, 1, 2, 3, 4, 5, 6, 7, 8]),
            prob( holds_at([fluent:located, pers:bob, loc:garden], tp:T), P) ),
          Result),
  print_result(Result).

test2n :-
  findall(neg(holds_at([fluent:located, pers:bob, loc:garage], tp:T)):P,
      ( member(T, [0, 1, 2, 3, 4, 5, 6, 7, 8]),
            prob( neg(holds_at([fluent:located, pers:bob, loc:garage], tp:T)), P) ),
          Result),
  print_result(Result).


% ---------------

test3 :-
  findall(holds_at([fluent:located, pers:bob, loc:kitchen], tp:T):P,
      ( member(T, [0, 1, 2, 3, 4, 5, 6, 7, 8]),
            prob( holds_at([fluent:located, pers:bob, loc:kitchen], tp:T), P) ),
          Result),
  print_result(Result).

test3n :-
  findall(neg(holds_at([fluent:located, pers:bob, loc:kitchen], tp:T)):P,
      ( member(T, [0, 1, 2, 3, 4, 5, 6, 7, 8]),
            prob( neg(holds_at([fluent:located, pers:bob, loc:kitchen], tp:T)), P) ),
          Result),
  print_result(Result).


% ---------------

test4 :-
  findall(holds_at([fluent:located, obj:screwdriver, loc:kitchen], tp:T):P,
      ( member(T, [0, 1, 2, 3, 4, 5, 6, 7, 8]),
            prob( holds_at([fluent:located, obj:screwdriver, loc:kitchen], tp:T), P) ),
          Result),
  print_result(Result).

test4n :-
  findall(neg(holds_at([fluent:located, obj:screwdriver, loc:kitchen], tp:T)):P,
      ( member(T, [0, 1, 2, 3, 4, 5, 6, 7, 8]),
            prob( neg(holds_at([fluent:located, obj:screwdriver, loc:kitchen], tp:T)), P) ),
          Result),
  print_result(Result).


% ---------------

test5 :-
    prob( holds_at([fluent:located, pers:Who, loc:kitchen], tp:3), P),
    write(P), nl,
    write(Who).

test6 :-
    prob( holds_at([fluent:located, pers:bob, loc:Where], tp:3), P),
    write(P), nl,
    write(Where).

test7 :-
    prob( holds_at([fluent:located, pers:Who, loc:Where], tp:3), P),
    write(P), nl,
    write(Who), nl,
    write(Where).

test8 :-
    prob( holds_at([fluent:What, pers:bob, loc:kitchen], tp:3), P),
    write(P), nl,
    write(What), nl.

test9 :-
    prob( holds_at([fluent:What, pers:Who, loc:Where], tp:3), P),
    write(P), nl,
    write(What), nl,
    write(Who), nl,
    write(Where), nl.

test10 :-
    member(When, [0, 1, 2, 3, 4, 5, 6, 7, 8]),
    prob( holds_at([fluent:located, pers:bob, loc:kitchen], tp:When), P),
    write(P), nl,
    write(When), nl.

test11 :-
    member(When, [0, 1, 2, 3, 4, 5, 6, 7, 8]),
    prob( holds_at([fluent:What, pers:Who, loc:Where], tp:When), P),
    write(P), nl,
    write(What), nl,
    write(Who), nl,
    write(Where), nl,
    write(When), nl.

% ---------------

test12 :-
  findall(holds_at([fluent:located, obj:orange, loc:garage], tp:T):P,
      ( member(T, [10,11,12,13,14,15,16,17]),
            prob( holds_at([fluent:located, obj:orange, loc:garage], tp:T), P) ),
          Result),
  print_result(Result).

test13 :-
  findall(holds_at([fluent:fixed, obj:car], tp:T):P,
      ( member(T, [4,5,6,7,8,9,10,11,12,13,14,15,16,17]),
            prob( holds_at([fluent:fixed, obj:car], tp:T), P) ),
          Result),
  print_result(Result).


test14 :-
  findall(holds_at([fluent:located, pers:bob, loc:store], tp:T):P,
      ( member(T, [4,5,6,7,8,9,10,11,12,13,14,15,16,17]),
            prob( holds_at([fluent:located, pers:bob, loc:store], tp:T), P) ),
          Result),
  print_result(Result).

test15 :-
  findall(holds_at([fluent:broken, obj:car], tp:T):P,
      ( member(T, [4,5,6,7,8,9,10,11,12,13,14,15,16,17]),
            prob( holds_at([fluent:broken, obj:car], tp:T), P) ),
          Result),
  print_result(Result).

test16 :-
  findall(holds_at([fluent:located, pers:bob, loc:garage], tp:T):P,
      ( member(T, [4,5,6,7,8,9,10,11,12,13,14,15,16,17]),
            prob( holds_at([fluent:located, pers:bob, loc:garage], tp:T), P) ),
          Result),
  print_result(Result).

% ----------------------------------------------------------------------
% Print result
% ----------------------------------------------------------------------

test_ec_pita([P1, P2, P3, P4, P5, P6, P7, P8, P9, P10]) :-
  prob( holds_at([fluent:located, pers:bob, loc:garden],  tp:3),  P1),
  prob( holds_at([fluent:located, pers:bob, loc:kitchen],  tp:5),  P2),
  prob( holds_at([fluent:holding, pers:bob, obj:screwdriver],  tp:7),  P3),
  prob( holds_at([fluent:located, pers:bob, loc:garage], tp:7),  P4),
  prob( holds_at([fluent:damaged, obj:car], tp:7),  P5),
  prob( holds_at([fluent:located, pers:bob, loc:kitchen], tp:10), P6),
  prob( holds_at([fluent:located, pers:bob, loc:store], tp:11), P7),
  prob( holds_at([fluent:carrying, pers:bob, obj:apple],  tp:12), P8),
  prob( holds_at([fluent:broken, obj:car],  tp:14), P9),
  prob( holds_at([fluent:located, obj: orange, loc:garage],  tp:15), P10).
          

print_result([]).
print_result([Result|Results]) :-
  write(Result),
  nl,
  print_result(Results).