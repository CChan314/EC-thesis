% ======================================================================
% Tiny Probabilistic Event Calculus
%
% Authors:
%   Rolf Schwitter   (Macquarie University, Australia)
%   Fabrizio Riguzzi (University Ferrara, Italy)
%   Christopher Chan (Macquarie University, Australia)
%
% Date:
%   15th August 2017
% ======================================================================

:- discontiguous initiated_at/2.
:- discontiguous terminated_at/2.
% ----------------------------------------------------------------------
% Domain-independent axioms
% ----------------------------------------------------------------------

holds_at(F, tp:T) :-
  initially(F),
  \+ clipped(tp:0, F, tp:T).

holds_at(F, tp:T2) :-
  initiated_at(F, tp:T1),
  T1 < T2,
  \+ clipped(tp:T1, F, tp:T2).

clipped(tp:T1, F, tp:T3) :-
  terminated_at(F, tp:T2),
  T1 < T2, T2 < T3.

% ----------------------------------------------------------------------
% Initial situation
% ----------------------------------------------------------------------

initially([fluent:located, pers:bob, loc:livingroom]).
initially([fluent:located, obj:book, loc:livingroom]).


% ----------------------------------------------------------------------
% events
% ----------------------------------------------------------------------

happens_at([event:pickUp, pers:bob, obj:book], tp:2).
happens_at([event:walk, pers:bob, loc:kitchen], tp:3).
happens_at([event:putDown, pers:bob, obj:book], tp:4).

% ----------------------------------------------------------------------
% Probabilistic effect axioms
%   - output of: learn_effect_axioms.pl
% ----------------------------------------------------------------------

initiated_at([fluent:located, pers:A, loc:B], tp:D):-
  happens_at([event:walk, pers:A, loc:B], tp:D).

terminated_at([fluent:located, pers:A, loc:B], tp:E) :-
  happens_at([event:walk, pers:A, loc:_], tp:E),
  location([loc: B]).


% ----------------------------------------------------------------------
% Additional effects
%
% ----------------------------------------------------------------------

terminated_at([fluent:carrying, pers:A, obj:B], tp: C):-
    happens_at([event:putDown,pers:A,obj:B], tp: C).

initiated_at([fluent:located, obj: A, loc:B], tp:C) :-
    happens_at([event:walk,pers:D, loc:B], tp:C),
    holds_at([fluent:carrying,pers:D, obj:A], tp:C).

initiated_at([fluent:carrying, pers:A, obj:B], tp:C):-
    happens_at([event:pickUp, pers:A,obj:B], tp: C).

terminated_at([fluent:located, obj: A, loc:B], tp:C):-
    happens_at([event:walk, pers:D, loc:_], tp:C),
    location([loc: B]),
    holds_at([fluent:carrying,pers:D,obj:A],tp:C).

% ----------------------------------------------------------------------
% Auxiliary facts to guarantee range restriction
% ----------------------------------------------------------------------

location([loc: livingroom]).
location([loc: kitchen]).
