% ======================================================================
% Tiny Probabilistic Event Calculus
%
% Authors:
%   Rolf Schwitter   (Macquarie University, Australia)
%   Fabrizio Riguzzi (University Ferrara, Italy)
%   Christopher Chan (Macquarie University, Australia)
%
% Date:
%   15th August 2017
% ======================================================================
:- discontiguous initiated_at/2.
:- discontiguous terminated_at/2.

% ----------------------------------------------------------------------
% Domain-independent axioms
% ----------------------------------------------------------------------

holds_at(F, tp:T) :-
  initially(F),
  write("F: ", F),
  \+ clipped(tp:0, F, tp:T),
  write("F2: ", F).

holds_at(F, tp:T2) :-
  initiated_at(F, tp:T1),
  T1 < T2,
  \+ clipped(tp:T1, F, tp:T2).

clipped(tp:T1, F, tp:T3) :-
  terminated_at(F, tp:T2),
  T1 < T2, T2 < T3.

% ----------------------------------------------------------------------
% Initial situation
% ----------------------------------------------------------------------

initially([fluent:located, pers:bob, loc:livingroom]).
initially([fluent:located, obj:device, loc:livingroom]).
initially([fluent:off, obj:device]).
initially([fluent:switchedOff, obj:device]).
initially([fluent:unplugged, obj:device]).


% ----------------------------------------------------------------------
% events
% ----------------------------------------------------------------------

happens_at([event:switchesOn, pers:bob, obj:device], tp:2).
happens_at([event:plugsIn, pers:bob, obj:device], tp:5).
happens_at([event:unplugs, pers:bob, obj:device], tp: 7).

% ----------------------------------------------------------------------
% Probabilistic effect axioms
%   - output of: learn_effect_axioms.pl
% ----------------------------------------------------------------------

initiated_at([fluent:located, pers:A, loc:B], tp:D):-
  happens_at([event:walk, pers:A, loc:B], tp:D).

terminated_at([fluent:located, pers:A, loc:B], tp:E) :-
  happens_at([event:walk, pers:A, loc:_], tp:E),
  location([loc: B]).

% ----------------------------------------------------------------------
% Additional effects
%
% ----------------------------------------------------------------------

initiated_at([fluent:on, obj:device],tp:T) :-
    holds_at([fluent:switchedOn, obj:device], tp:T),
    happens_at([event:plugsIn, pers: _, obj:device],tp:T).

initiated_at([fluent:switchedOn, obj:device],tp:T) :-
    happens_at([event:switchesOn, pers: _, obj:device],tp:T).

initiated_at([fluent:pluggedIn, obj:device],tp:T) :-
    happens_at([event:plugsIn, pers: _, obj:device],tp:T).

terminated_at([fluent:switchedOn, obj:device],tp:T) :-
    happens_at(neg([event:switchesOn, pers: _, obj:device]),tp:T).

terminated_at([fluent:pluggedIn, obj:device],tp:T) :-
    happens_at(neg([event:plugsIn, pers: _, obj:device]),tp:T).


% ----------------------------------------------------------------------
% Auxiliary facts to guarantee range restriction
% ----------------------------------------------------------------------

location([loc: livingroom]).