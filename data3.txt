(ROOT
  (S
    (NP (NNP Mary))
    (VP (VBZ is)
      (VP (VBN located)
        (PP (IN in)
          (NP (DT the) (NN yard)))))
    (. .)))

(ROOT
  (S
    (NP (NNP Mary))
    (VP (VBZ arrives)
      (PP (IN in)
        (NP (DT the) (NN kitchen))))
    (. .)))

(ROOT
  (SBARQ
    (WHADVP (WRB Where))
    (SQ (VBZ is)
      (NP (NNP Mary)))
    (. ?)))
(ROOT
  (S
    (NP (NNP Mary))
    (VP (VBZ is)
      (PP (IN in)
        (NP (DT the) (NN kitchen))))
    (. .)))


